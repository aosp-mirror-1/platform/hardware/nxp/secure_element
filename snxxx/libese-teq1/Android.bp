package {
    // See: http://go/android-license-faq
    // A large-scale-change added 'default_applicable_licenses' to import
    // all of the 'license_kinds' from "hardware_nxp_secure_element_license"
    // to get the below license kinds:
    //   SPDX-license-identifier-Apache-2.0
    default_applicable_licenses: ["hardware_nxp_secure_element_license"],
}

cc_library_shared {

    name: "ese_teq1_nxp_snxxx",
    defaults: ["hidl_defaults"],
    proprietary: true,

    srcs: [
        "nxp-ese/lib/phNxpEseDataMgr.cpp",
        "nxp-ese/lib/phNxpEseProto7816_3.cpp",
        "nxp-ese/lib/phNxpEse_Apdu_Api.cpp",
        "nxp-ese/lib/phNxpEse_Api.cpp",
        "nxp-ese/pal/phNxpEsePal.cpp",
        "nxp-ese/pal/EseTransportFactory.cpp",
        "nxp-ese/pal/spi/EseSpiTransport.cpp",
        "nxp-ese/pal/NxpTimer.cpp",
        "nxp-ese/spm/phNxpEse_Spm.cpp",
        "nxp-ese/utils/ese_config.cpp",
        "nxp-ese/utils/config.cpp",
        "nxp-ese/utils/ringbuffer.cpp",
        "src/adaptation/NfcAdaptation.cpp",
        "src/adaptation/CondVar.cpp",
        "src/adaptation/Mutex.cpp",
    ],

    local_include_dirs: [
        "nxp-ese/lib",
        "nxp-ese/pal/spi",
        "nxp-ese/utils",
    ],
    export_include_dirs: [
        "common/include",
        "nxp-ese/common",
        "nxp-ese/inc",
        "nxp-ese/pal",
        "src/include",
    ],
    include_dirs: [
        "hardware/nxp/nfc/snxxx/extns/impl/nxpnfc/aidl",
        "hardware/nxp/nfc/snxxx/extns/impl/nxpnfc/2.0",
        "hardware/nxp/secure_element/snxxx/extns/impl",
        "hardware/nxp/secure_element/snxxx/ese-clients/inc",
    ],

    cflags: [
        "-DANDROID",
        "-DJCOP_VER_3_1=1",
        "-DJCOP_VER_3_2=2",
        "-DJCOP_VER_3_3=3",
        "-DJCOP_VER_4_0=4",
        "-DJCOP_VER_5_x=5",
        "-DBUILDCFG=1",
        "-DNXP_EXTNS=TRUE",
        "-DNFC_NXP_ESE_VER=JCOP_VER_5_x",
        "-Wall",
        "-Werror",
    ],

    shared_libs: [
        "android.hardware.nfc@1.0",
        "android.hardware.nfc@1.1",
        "android.hardware.secure_element@1.0",
        "libcutils",
        "libhardware",
        "libhidlbase",
        "libutils",
        "libbinder",
        "libbinder_ndk",
        "liblog",
        "libbase",
        "vendor.nxp.nxpese@1.0",
        "vendor.nxp.nxpnfc@2.0",
        "vendor.nxp.nxpnfc_aidl-V1-ndk",
    ],

    product_variables: {
        debuggable: {
            cflags: [
                "-DDCHECK_ALWAYS_ON",
            ],
        },
    },

}
